from gym.envs.toy_text.frozen_lake import generate_random_map
import random
import numpy as np

random.seed(11041996)
np.random.seed(11041996)

sixteens = generate_random_map(16)
thirtytwos = generate_random_map(32)
MAPS = {
    '32x32': thirtytwos,
    '16x16': sixteens,
    '8x8': [
        'SFFFFFFF',
        'FFFFFFFF',
        'FFFHFFFF',
        'FFFFFHFF',
        'FFFHFFFF',
        'FHHFFFHF',
        'FHFFHFHF',
        'FFFHFFFG'
    ],
    '4x4': [
        'SFFF',
        'FHFH',
        'FFFH',
        'HFFG'
    ]
}
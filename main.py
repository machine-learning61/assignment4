from hiive.mdptoolbox import mdp, example
import numpy as np
import pandas as pd
import time
# import hiive.mdptoolbox
import random
import gym
from openai import OpenAI_MDPToolbox
import matplotlib.pyplot as plt
import os

from gym.envs.toy_text.frozen_lake import generate_random_map, FrozenLakeEnv

from plot import see_policy, plot_metric
from utils import MAPS

random.seed(11041996)
np.random.seed(11041996)

FROZEN_LAKE = 'frozen-lake'
FOREST = 'forest'

# TODO: Run at 1e6
MAX_ITER = int(1e3)

def time_experiment(P, R, num_states, problem_type):
    print('time experiment')
    vi_times = []
    pi_times = []
    ql_times = []
    for gamma in [.2, .4, .6, .8, .999]:
        print(gamma)

        vi = mdp.ValueIteration(P, R, max_iter=MAX_ITER, gamma=gamma)
        start = time.time()
        temp_df = pd.DataFrame(vi.run())
        end = time.time()
        vi_times.append(end - start)

        pi = mdp.PolicyIteration(P, R, max_iter=MAX_ITER, gamma=gamma)
        start = time.time()
        pi.setVerbose()
        temp_df = pd.DataFrame(pi.run())
        end = time.time()
        pi_times.append(end-start)

        q = mdp.QLearning(P, R, gamma)
        q.Q = np.random.rand(q.S, q.A)
        start = time.time()
        temp_df = pd.DataFrame(q.run())
        end = time.time()
        ql_times.append(end-start)

    plot_name = 'frozen-lake-{}x{}-time-to-converge'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'forest-{}-time-to-converge'.format(num_states)
    plt.figure(plot_name)
    plt.title(plot_name)
    plt.plot([.2, .4, .6, .8, .999], vi_times, label="VI")
    plt.plot([.2, .4, .6, .8, .999], pi_times, label="PI")
    plt.plot([.2, .4, .6, .8, .999], ql_times, label="QL")
    plt.xlabel('Gamma')
    plt.ylabel('Time to Converge')
    plt.legend()
    plt.savefig('plots/{}/compare/{}'.format(problem_type, plot_name))	
    plt.close()

def q_learning(P, R, num_states, problem_type):
    f = open('results.txt', "a")
    f.write('------Q LEARNING------\n')
    df = pd.DataFrame()
    
    test_q = mdp.QLearning(P, R, 0.999)
    initial_q_policy = np.random.rand(test_q.S, test_q.A)

    # Decay Experiments
    decay_vals = [.01, .1, .5, .9]
    policies = {}
    GAMMA = 0.999
    for alpha in decay_vals:
        q = mdp.QLearning(P, R, GAMMA, alpha_decay=alpha)
        q.Q = initial_q_policy#np.random.rand(q.S, q.A)
        temp_df = pd.DataFrame(q.run())
        f.write('alpha_decay: {} | Policy: {}\n'.format(alpha, q.policy))

        temp_df["alpha_decay"] = alpha
        df = pd.concat([df, temp_df])

        policies[alpha] = np.array(q.policy)

        if problem_type == FROZEN_LAKE:
            policy_plot_name = 'q-heatmap-{}x{}-alpha-{}'.format(num_states, num_states, int(alpha * 1000))
            see_policy(num_states, q.policy, policy_plot_name, 'q')
        
    # Mean V
    plot_name = 'q-alpha-tune-frozen-lake-{}x{}-mean-v'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'q-alpha-tune-forest-{}-mean-v'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'alpha_decay', 'metric': 'Mean V', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df, params, decay_vals, problem_type, 'q')

    # Reward
    plot_name = 'q-alpha-tune-frozen-lake-{}x{}-reward'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'q-alpha-tune-forest-{}-reward'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'alpha_decay', 'metric': 'Reward', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df, params, decay_vals, problem_type, 'q')

    # Error
    plot_name = 'q-alpha-tune-frozen-lake-{}x{}-error'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'q-alpha-tune-forest-{}-error'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'alpha_decay', 'metric': 'Error', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df, params, decay_vals, problem_type, 'q')

    # Policy Change Log
    change_percentage = np.sum(policies[0.1] != policies[0.9]) / len(policies[0.9]) * 100
    f.write('Alpha Policy Changed {}%\n'.format(str(change_percentage)))

    policies = {}

    for epsilon in decay_vals:
        q = mdp.QLearning(P, R, GAMMA, epsilon_decay=epsilon)
        q.Q = np.random.rand(q.S, q.A)
        temp_df = pd.DataFrame(q.run())
        f.write('epsilon_decay: {} | Policy: {}\n'.format(epsilon, q.policy))

        temp_df["epsilon_decay"] = epsilon
        df = pd.concat([df, temp_df])

        policies[epsilon] = np.array(q.policy)

        if problem_type == FROZEN_LAKE:
            policy_plot_name = 'q-heatmap-frozen-lake-{}x{}-epsilon-{}'.format(num_states, num_states, int(epsilon * 1000))
            see_policy(num_states, q.policy, policy_plot_name, 'q')
        
    # Mean V
    plot_name = 'q-epsilon-tune-frozen-lake-{}x{}-mean-v'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'q-epsilon-tune-forest-{}-mean-v'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'epsilon_decay', 'metric': 'Mean V', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df, params, decay_vals, problem_type, 'q')

    # Reward
    plot_name = 'q-epsilon-tune-frozen-lake-{}x{}-reward'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'q-epsilon-tune-forest-{}-reward'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'epsilon_decay', 'metric': 'Reward', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df, params, decay_vals, problem_type, 'q')

    # Error
    plot_name = 'q-epsilon-tune-frozen-lake-{}x{}-error'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'q-epsilon-tune-forest-{}-error'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'epsilon_decay', 'metric': 'Error', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df, params, decay_vals, problem_type, 'q')

    # Policy Change Log
    # change_percentage = np.sum(policies[0.7] != policies[0.99]) / len(policies[0.9]) * 100
    # f.write('epsilon Policy Changed {}%\n'.format(str(change_percentage)))
    
    f.close()


def state_size_experiment(problem_type):
    print('State Size Experiment: ', problem_type)
    # Frozen Lake
    # TODO: Do a run with 32
    sizes = [4, 8, 16, 32] if problem_type == FROZEN_LAKE else np.arange(200, 1000, 200)
    df_vi = pd.DataFrame()
    df_pi = pd.DataFrame()

    vi_times = []
    pi_times = []

    GAMMA = 0.999
    for problem_size in sizes:
        print('problem_size', problem_size)
        P = None
        R = None
        if problem_type == FROZEN_LAKE:
            lookup_string = '{}x{}'.format(problem_size, problem_size)
            problem = OpenAI_MDPToolbox('FrozenLake-v1', is_slippery=True, desc=MAPS[lookup_string])
            P = problem.P
            R = problem.R
        else:
            P, R = example.forest(S=problem_size)


        # VI
        vi = mdp.ValueIteration(P, R, max_iter=MAX_ITER, gamma=GAMMA)
        start = time.time()
        temp_df = pd.DataFrame(vi.run())
        end = time.time()
        temp_df["problem_size"] = problem_size
        df_vi = pd.concat([df_vi, temp_df])
        vi_times.append(end-start)

        # PI
        pi = mdp.PolicyIteration(P, R, max_iter=MAX_ITER, gamma=GAMMA)
        pi.setVerbose()
        start = time.time()
        temp_df = pd.DataFrame(pi.run())
        end = time.time()
        temp_df["problem_size"] = problem_size
        df_pi = pd.concat([df_pi, temp_df])
        pi_times.append(end-start)
    
    plot_name = 'vi-problem-size-mean-v'
    plt.figure(plot_name)
    plt.title(plot_name)
    for problem_size in sizes:
        plt.plot(df_vi[df_vi['problem_size']==problem_size]['Mean V'], label="problem_size={}x{}".format(problem_size, problem_size))
    plt.legend()
    plt.title('Change in Mean V for different Problem Sizes')
    plt.xlabel('Iterations')
    plt.ylabel('Mean V')
    plt.savefig('plots/{}/vi/{}'.format(problem_type, plot_name))	
    plt.close()

    plot_name = 'pi-problem-size-mean-v'
    plt.figure(plot_name)
    plt.title(plot_name)
    for problem_size in sizes:
        plt.plot(df_pi[df_pi['problem_size']==problem_size]['Mean V'], label="problem_size={}x{}".format(problem_size, problem_size))
    plt.legend()
    plt.title('Change in Mean V for different Problem Sizes')
    plt.xlabel('Iterations')
    plt.ylabel('Mean V')
    plt.savefig('plots/{}/pi/{}'.format(problem_type, plot_name))	
    plt.close()

    # Time
    plot_name = 'problem-size-time'
    plt.figure(plot_name)
    plt.title(plot_name)
    plt.plot(sizes, vi_times, label="VI")
    plt.plot(sizes, pi_times, label="PI")
    plt.legend()
    plt.title('Time to Converge for different Problem Sizes')
    plt.xlabel('Problem Size')
    plt.ylabel('Time to Converge')
    plt.savefig('plots/{}/compare/{}'.format(problem_type, plot_name))	
    plt.close()


def main_experiment(P, R, num_states, problem_type):
    df_vi = pd.DataFrame()
    df_pi = pd.DataFrame()

    vi_policies = {}
    pi_policies = {}

    gamma_vals = [0.9, 0.99, 0.999]
    f = open('results.txt', "a")

    for gamma in gamma_vals:
        print('Gamma:', gamma)
        f.write('Gamma: {}\n'.format(gamma))
        vi = mdp.ValueIteration(P, R, max_iter=MAX_ITER, gamma=gamma)
        temp_df = pd.DataFrame(vi.run())
        print('vi done')
        f.write('VI Policy: {}\n'.format(vi.policy))

        temp_df["gamma"] = gamma
        df_vi = pd.concat([df_vi, temp_df])
        vi_policies[gamma] = np.array(vi.policy)
        
        pi = mdp.PolicyIteration(P, R, max_iter=MAX_ITER, gamma=gamma)
        pi.setVerbose()
        temp_df = pd.DataFrame(pi.run())
        print('pi done')
        f.write('PI Policy: {}\n\n'.format(pi.policy))
        temp_df["gamma"] = gamma

        df_pi = pd.concat([df_pi, temp_df])
        pi_policies[gamma] = np.array(pi.policy)
        
        if problem_type == FROZEN_LAKE:
            policy_plot_name = 'vi-heatmap-{}x{}-{}'.format(num_states, num_states, int(gamma * 1000))
            see_policy(num_states, vi_policies[gamma], policy_plot_name, 'vi')
            
            policy_plot_name = 'pi-heatmap-{}x{}-{}'.format(num_states, num_states, int(gamma * 1000))
            see_policy(num_states, pi_policies[gamma], policy_plot_name, 'pi')
        


    
    # Mean V
    plot_name = 'vi-{}x{}-mean-v'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'vi-{}-mean-v'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Mean V', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df_vi, params, gamma_vals, problem_type, 'vi')

    plot_name = 'pi-{}x{}-mean-v'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'pi-{}-mean-v'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Mean V', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df_pi, params, gamma_vals, problem_type, 'pi')

    # # Reward
    # plot_name = 'vi-{}x{}-reward'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'vi-{}-reward'.format(num_states)
    # params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Reward', 'title': plot_name, 'xaxis': 'Iterations'}
    # plot_metric(df_vi, params, gamma_vals, problem_type, 'vi')

    # plot_name = 'pi-{}x{}-reward'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'pi-{}-reward'.format(num_states)
    # params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Reward', 'title': plot_name, 'xaxis': 'Iterations'}
    # plot_metric(df_pi, params, gamma_vals, problem_type, 'pi')

    # Time
    plot_name = 'vi-{}x{}-time'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'vi-{}-time'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Time', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df_vi, params, gamma_vals, problem_type, 'vi')

    plot_name = 'pi-{}x{}-time'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'pi-{}-time'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Time', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df_pi, params, gamma_vals, problem_type, 'pi')

    # Comparative Mean V
    plot_name = '{}x{}-mean-v'.format(num_states, num_states) if problem_type == FROZEN_LAKE else '{}-mean-v'.format(num_states)
    plt.figure(plot_name)
    plt.title(plot_name)
    plt.plot(df_vi[df_vi['gamma']==0.999]['Mean V'], label="VI")
    plt.plot(df_pi[df_pi['gamma']==0.999]['Mean V'], label="PI")
    plt.legend()
    plt.title(plot_name)
    plt.xlabel('Iterations')
    plt.ylabel('V Mean')
    plt.savefig('plots/{}/compare/{}'.format(problem_type, plot_name))	
    plt.close()


    # Error
    plot_name = 'vi-{}x{}-error'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'vi-{}-error'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Error', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df_vi, params, gamma_vals, problem_type, 'vi')

    plot_name = 'pi-{}x{}-error'.format(num_states, num_states) if problem_type == FROZEN_LAKE else 'pi-{}-error'.format(num_states)
    params = {'plot_name': plot_name, 'hyperparam': 'gamma', 'metric': 'Error', 'title': plot_name, 'xaxis': 'Iterations'}
    plot_metric(df_pi, params, gamma_vals, problem_type, 'pi')

    # Log how Policies changed for diff Gamma Values
    # VI
    vi_change_percentage = np.sum(vi_policies[0.9] != vi_policies[0.999]) / len(vi_policies[0.9]) * 100
    pi_change_percentage = np.sum(pi_policies[0.9] != pi_policies[0.999]) / len(pi_policies[0.9]) * 100
    vipi_diff = np.sum(vi_policies[0.999] != pi_policies[0.999]) / len(pi_policies[0.999]) * 100
    f.write('VI Policy Changed {}%\n'.format(str(vi_change_percentage)))
    f.write('PI Policy Changed {}%\n'.format(str(pi_change_percentage)))
    f.write('VI PI Policy Difference {}%\n'.format(str(vipi_diff)))
    f.close()

def run_experiment(P, R, num_states, problem_type):
    f = open('results.txt', "a")
    f.write('------------\n')
    f.write('Problem Type: {} | State Size: {}\n\n'.format(problem_type, num_states))
    f.close()
    main_experiment(P, R, num_states, problem_type)
    q_learning(P, R, num_states, problem_type)
    time_experiment(P, R, num_states, problem_type)
   
def set_up_plot_dirs():
    top_level = ['frozen-lake', 'forest']
    bottom_level = ['compare', 'pi', 'q', 'vi']

    for top in top_level:
        for bottom in bottom_level:
            path = 'plots/{}/{}'.format(top, bottom)        
            if not os.path.exists(path):
                os.makedirs(path)

def main():
    print('Assignment 4')
    f = open('results.txt', "w")
    f.write('Assignment 4 Results\n\n')
    f.close()

    set_up_plot_dirs()


    S = 20
    P, R = example.forest(S=S)
    run_experiment(P, R, S, FOREST)

    # S = 10000
    S = 1000
    P, R = example.forest(S=S)
    run_experiment(P, R, S, FOREST)

    problem = OpenAI_MDPToolbox('FrozenLake-v1', is_slippery=True)
    run_experiment(problem.P, problem.R, int(np.sqrt(problem.states)), FROZEN_LAKE)
    problem = OpenAI_MDPToolbox('FrozenLake-v1', desc=MAPS['16x16'], is_slippery=True)
    run_experiment(problem.P, problem.R, int(np.sqrt(problem.states)), FROZEN_LAKE)

    state_size_experiment(FROZEN_LAKE)
    state_size_experiment(FOREST)
   
if __name__ == '__main__':
    main()

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from gym.envs.toy_text.frozen_lake import generate_random_map, FrozenLakeEnv
from seaborn import heatmap
from utils import MAPS

def map_discretize(the_map):
    size = len(the_map)
    dis_map = np.zeros((size,size))
    for i, row in enumerate(the_map):
        for j, loc in enumerate(row):
            if loc == "S":
                dis_map[i, j] = 0
            elif loc == "F":
                dis_map[i, j] = 0
            elif loc == "H":
                dis_map[i, j] = -1
            elif loc == "G":
                dis_map[i, j] = 1
    return dis_map


def policy_numpy(policy):
    size = int(np.sqrt(len(policy)))
    pol = np.asarray(policy)
    pol = pol.reshape((size, size))
    return pol


def see_policy(map_size, policy, plot_name, learner_type):
    map_name = str(map_size)+"x"+str(map_size)
    data = map_discretize(MAPS[map_name])
    np_pol = policy_numpy(policy)
    plt.figure(plot_name)
    plt.imshow(data, interpolation="nearest")

    for i in range(np_pol[0].size):
        for j in range(np_pol[0].size):
            arrow = '\u2190'
            if np_pol[i, j] == 1:
                arrow = '\u2193'
            elif np_pol[i, j] == 2:
                arrow = '\u2192'
            elif np_pol[i, j] == 3:
                arrow = '\u2191'
            text = plt.text(j, i, arrow,
                           ha="center", va="center", color="w")
    # plt.show()
    plt.title(plot_name)
    
    plt.savefig('plots/frozen-lake/{}/{}'.format(learner_type, plot_name))	
    plt.close()


def plot_metric(df, params, gamma_vals, problem_type, learner_type):
    plt.figure(params['plot_name'])
    plt.title(params['plot_name'])
    for gamma in gamma_vals:
        plt.plot(df[df[params['hyperparam']]==gamma][params['metric']], label="{}={}".format(params['hyperparam'], gamma))
    plt.legend()
    plt.title(params['title'])
    plt.xlabel(params['xaxis'])
    plt.ylabel(params['metric'])
    if learner_type == 'pi':
        plt.xlim(0, 100)
    plt.savefig('plots/{}/{}/{}'.format(problem_type, learner_type, params['plot_name']))	
    plt.close()
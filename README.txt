Link to code repo: https://gitlab.com/machine-learning61/assignment4


3rd Party Libraries Used:
- MDPToolbox - HIIVE, For all learners and Forest MDP
- OpenAI Gym, for Frozen Lake

Python version used: Python 3.9.13

How to run Code:
    1. Create virtual environment: python3 -m venv env
    2. Activate virtual environment: source env/bin/activate
    3. Install dependencies: pip install -r requirements.txt
    4. Run code: python main.py
            